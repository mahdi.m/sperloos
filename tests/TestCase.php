<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication
        , RefreshDatabase;


    /**
     * apply actingAs method for passport
     * actually login user
     */
    public function passportLogin()
    {
        return Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

}
