# The work done

### Add these lines to .env

    TEST_DB_HOST=localhost
    TEST_DB_DATABASE=sperloos_testing_db
    TEST_DB_USERNAME=root
    TEST_DB_PASSWORD=123456

### create "sperloos_testing_db" database

### Run this command to test app

    php artisan test ./Modules/Panel/Tests/Feature

