<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'path' => 'array'
    ];

    /**
     * delete file from disk
     * @return bool|void|null
     */
    public function delete()
    {
        foreach ($this->path as $index => $path) {
            $exists = Storage::disk('public')->exists($path);
            if ($exists) {
                Storage::disk('public')->delete($path);
            }
        }
        parent::delete();
    }


    public function imageable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }
}
