<?php

if (!function_exists('createUserToken')) {
    /**
     * create user login token
     *
     * ایجاد توکن ورورد برای کاربر
     *
     * @param string $email
     * @return void
     */
    function createUserToken(string $email)
    {
        $user = \App\Models\User::whereEmail($email)->first();
        return $user->createToken('panel token')->accessToken;
    }
}
