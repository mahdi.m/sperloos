<?php

namespace Modules\Panel\Http\Controllers;

use App\Facades\ResponderProviderFacade;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panel\Entities\Post;
use Modules\Panel\Http\Requests\PostRequest;
use Modules\Panel\Services\Post\PostDestroyService;
use Modules\Panel\Services\Post\PostDisplayService;
use Modules\Panel\Services\Post\PostPaginateService;
use Modules\Panel\Services\Post\PostStoreService;
use Modules\Panel\Services\Post\PostUpdateService;
use Modules\Panel\Transformers\PostResource;

/**
 * @group Panel
 * @authenticated
 */
class PostController extends Controller
{
    /**
     * Post displays all.
     * @param PostPaginateService $postPaginateService
     * @return mixed
     */
    public function index(PostPaginateService $postPaginateService)
    {
        return $postPaginateService->index();
    }

    /**
     * Post paginate.
     *
     * @urlParam perPage int count in per pge
     * @param PostPaginateService $postPaginateService
     * @param null $perPage
     * @return mixed|PostResource
     */
    public function paginate(PostPaginateService $postPaginateService, $perPage = null)
    {
        if (!$perPage) return $this->index($postPaginateService);
        return PostResource::collection($postPaginateService->paginate((int)$perPage));
    }

    /**
     * Post store.
     *
     * @bodyParam title string required
     * @bodyParam content string required
     * @bodyParam image file required
     * @param PostRequest $request
     * @param PostStoreService $postStoreService
     * @return PostResource
     */
    public function store(PostRequest $request, PostStoreService $postStoreService)
    {
        $user = request()->user();
        if (!$user->can('store', Post::class)) return ResponderProviderFacade::error(Response::HTTP_FORBIDDEN, __('messages.response.forbidden'));

        return $postStoreService->store($request);
    }

    /**
     *  Post display single _ panel.
     * @param Post $post
     * @param PostDisplayService $postDisplayService
     * @return Post
     */
    public function show(Post $post, PostDisplayService $postDisplayService): Post
    {
        return $postDisplayService->show($post);
    }

    /**
     * Post update _ panel.
     * @bodyParam title string required
     * @bodyParam content string required
     * @param PostRequest $request
     * @param Post $post
     * @param PostUpdateService $postUpdateService
     * @return Post
     */
    public function update(PostRequest $request, Post $post, PostUpdateService $postUpdateService): Post
    {
        return $postUpdateService->update($request, $post);
    }

    /**
     * Post destroy _ panel.
     * @param Post $post
     * @param PostDestroyService $postDestroyService
     * @return Response
     */
    public function destroy(Post $post, PostDestroyService $postDestroyService)
    {
        $postDestroyService->destroy($post);
        return ResponderProviderFacade::destroyed(__('messages.response.destroy'));
    }


}
