<?php

namespace Modules\Panel\Tests\Feature\Controllers\Resource;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Testing\Fluent\AssertableJson;
use Modules\Panel\Entities\Post;
use Modules\Panel\Entities\User;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PostControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testPaginate()
    {
        $this->passportLogin();
        Post::factory()->count(3)->create();
        $response = $this->json('GET', route('posts.paginate', ['perPage' => 5]));
        $this->assertInstanceOf(Collection::class, $response->getOriginalContent());
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'content',
                ],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);

        $response->assertOk();
    }

    public function testStore()
    {
        $user = $this->passportLogin();
        Storage::fake('avatars');

        $image = UploadedFile::fake()->image('avatar.jpg');
        $data = [
            'title' => 'test course title',
            'content' => 'content',
            'image' => $image
        ];
        $response = $this->json(
            'POST',
            route('posts.store'),
            $data
        );
        $data = Arr::except($data, 'image');
        $response
            ->assertCreated()
            ->assertJsonStructure([
                'title',
                'content',
            ]);
        $this->assertDatabaseHas(Post::class, $data);
    }

    public function testDisplay()
    {
        $user = $this->passportLogin();
        $post = Post::factory()->create();
        $response = $this->json('GET', route('posts.show', ['post' => $post->id]));
        $response
            ->assertJson(
                $post->toArray()
            )
            ->assertOk();
    }


    public function testUpdate()
    {
        $this->passportLogin();

        $post = Post::factory()->create();
        $image = UploadedFile::fake()->image('avatar.jpg');
        $data = [
            'title' => 'test course title',
            'content' => 'content',
            'image' => $image
        ];
        $response = $this->json('PUT', route('posts.update', ['post' => $post->id]), $data);
        $data = Arr::except($data, 'image');
        $response->assertJson(
            $data
        )->assertOk();
    }

    public function testDestroy()
    {
        $this->passportLogin();
        $post = Post::factory()->create();
        $response = $this->json('DELETE', route('posts.destroy', ['post' => $post->id]));
        $response
            ->assertJson(['message' => __('messages.response.destroy')])
            ->assertOk();
        $this
            ->assertDeleted($post);
    }


}
