<?php

namespace Modules\Panel\Repositories;

use App\Repositories\BaseRepository;
use Modules\Panel\Entities\Post;

class PostRepository extends BaseRepository
{
    public function model(): string
    {
        return Post::class;
    }


}
