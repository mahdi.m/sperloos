<?php

namespace Modules\Panel\Repositories;

use App\Repositories\BaseRepository;
use Modules\Panel\Entities\Image;

class ImageRepository extends BaseRepository
{
    public function model()
    {
        return Image::class;
    }

}
