<?php

namespace Modules\Panel\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Panel\Entities\Post;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can store the post.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can store the post.
     *
     * @param User $user
     * @return mixed
     */
    public function store(User $user)
    {
        return $user->id % 2 == 0;
    }

    /**
     * Determine whether the user can store the post.
     *
     * @param User $user
     * @return mixed
     */
    public function display(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->id % 2 == 0;

    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->id % 2 == 0;

    }
}
