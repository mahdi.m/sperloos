<?php


namespace Modules\Panel\Facades;



use App\Facades\BaseFacade;

/**
 * Class UserProviderFacade
 * this class UserProviderFacade::shouldProxyTo(Modules\Auth\Services\Provider\UserProvider\UserProvider::class); in Modules\Auth\Providers\AuthServiceProvider
 * @package Modules\Auth\Facades
 * @method findUserByEmail($email)
 * @method passwordVerify($requestPassword, $userPassword)
 * @method createUserToken($user)
 */
class UserProviderFacade extends BaseFacade
{
    const key = 'panel.userProvider';
}
