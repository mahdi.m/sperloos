<?php

namespace Modules\Panel\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Panel\Database\factories\UserFactory;

class User extends \App\Models\User
{
    use HasFactory;


    protected static function newFactory()
    {
        return UserFactory::new();
    }
}
