<?php

namespace Modules\Panel\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Panel\Database\factories\PostFactory;

class Post extends \App\Models\Post
{
    use HasFactory;


    protected static function newFactory()
    {
        return PostFactory::new();
    }
}
