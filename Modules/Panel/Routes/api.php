<?php

use Illuminate\Support\Facades\Route;
use Modules\Panel\Http\Controllers\PostController;
use Modules\Panel\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('panel')->group(function () {

    Route::post('login', [LoginController::class, 'login'])->name('panel.login');

    Route::middleware('auth:api')->group(function () {

        Route::get('posts/paginate/{perPage?}', [PostController::class, 'paginate'])->name('posts.paginate');
        Route::resource('posts', PostController::class);

    });
});
