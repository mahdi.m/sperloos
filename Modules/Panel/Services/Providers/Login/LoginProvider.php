<?php


namespace Modules\Panel\Services\Providers\Login;


use App\Facades\ResponderProviderFacade;
use Modules\Panel\Services\Providers\CommonProvider;

/**
 * Class UserProvider
 * @package Modules\Panel\Services\Provider
 */
class LoginProvider extends CommonProvider
{

    public function findUserByEmail($email)
    {
        return $this->repository->findUserByEmail($email);
    }

    public function passwordVerify($requestPassword, $userPassword)
    {
        if (!password_verify($requestPassword, $userPassword)) {
            return ResponderProviderFacade::unauthorizedError('گذرواژه اشتباه است');
        }
        return true;
    }

    public function createUserToken($user)
    {
        $token = $user->createToken('panel token', [$user->guard])->accessToken;
        $user->setAttribute('api_token', $token);
        return $user;
    }

}
