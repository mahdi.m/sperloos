<?php


namespace Modules\Panel\Services\Providers;


use App\Services\BaseService;
use App\Repositories\BaseRepository;
use Modules\Panel\Repositories\UserRepository;

class CommonProvider extends BaseService
{
    public BaseRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }


}
