<?php


namespace Modules\Panel\Services\Post;


use Modules\Panel\Entities\Post;

class PostDestroyService extends PostCommonService
{
    public function destroy(Post $post)
    {
        return $this->repository->delete($post);
    }
}
