<?php


namespace Modules\Panel\Services\Post;


use Illuminate\Http\Request;
use Modules\Panel\Entities\Post;

class PostUpdateService extends PostCommonService
{
    public function update(Request $request, Post $post)
    {
        $this->repository->update($request->except('image'), $post);
        $this->addMedia($post, $request);
        return $post;
    }
}
