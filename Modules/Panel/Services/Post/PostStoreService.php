<?php


namespace Modules\Panel\Services\Post;


use Illuminate\Http\Request;

class PostStoreService extends PostCommonService
{
    public function store(Request $request)
    {
        $post = $this->repository->store($request->except('image'));
        $this->addMedia($post, $request);
        return $post;
    }

}
