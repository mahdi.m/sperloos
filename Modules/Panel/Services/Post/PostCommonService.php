<?php


namespace Modules\Panel\Services\Post;


use App\Repositories\BaseRepository;
use App\Services\BaseService;
use Illuminate\Http\Request;
use Modules\Panel\Repositories\PostRepository;

class PostCommonService extends BaseService
{
    public BaseRepository $repository;

    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $post
     * @param Request $request
     */
    public function addMedia($post, Request $request): void
    {
        $post
            ->addMedia($request->image)
            ->toMediaCollection();
    }

}
