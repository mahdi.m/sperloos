<?php


namespace Modules\Panel\Services\Post;


use Modules\Panel\Entities\Post;

class PostDisplayService extends PostCommonService
{
    public function show(Post $post)
    {
        return $post;
    }
}
